from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class UnitTest(TestCase):

    def test_views(self):
        response = self.client.get("/")
        self.assertTemplateUsed("index.html")
        self.assertContains(response, "Activities", html=True)
        self.assertContains(response, "Experiences", html=True)
        self.assertContains(response, "Achievements", html=True)
        self.assertContains(response, "Educations", html=True)
        target = resolve("/")
        self.assertTrue(target.func, index)
